import java.util.Arrays;
import java.util.Scanner;

public class Student {
    public String getName() {
        return name;
    }

    private String name;

    public String getCode() {
        return code;
    }

    private String code;

    public Integer getGrade() {
        return grade;
    }

    private Integer grade;

    public Student(String name, Integer grade, String code) {
        this.name = name;
        this.grade = grade;
        this.code = code;
    }

    public String toString() {
        return String.format("('%s', %d, '%s')", name, grade, code);
    }

    public static void main(String[] args) {
        Student[] A = new Student[5];
        A[0] = new Student("Maria", 15, "CS2001");
        A[1] = new Student("Juan", 12, "CS2002");
        A[2] = new Student("Diego", 20, "CS2014");
        A[3] = new Student("Aldo", 18, "CS2004");
        A[4] = new Student("Carlos", 18, "CS2014");
        StudentSorter sorter = new StudentSorter();
        Scanner sc = new Scanner(System.in);
        String field = sc.next();
        if (field.equals("name"))
            sorter.sort(A, new StudentOrderByName());
        else if (field.equals("grade"))
            sorter.sort(A, new StudentOrderByGrade());
        System.out.println(Arrays.deepToString(A));
    }
}
