public class StudentOrderByGrade implements StudentOrder {
    @Override
    public boolean compare(Student a, Student b) {
        return a.getGrade() < b.getGrade();
    }
}
