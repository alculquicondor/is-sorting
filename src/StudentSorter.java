public class StudentSorter {
    private Student[] A;
    private StudentOrder order;

    public void sort(Student[] A, StudentOrder order) {
        this.A = A;
        this.order = order;
        quickSort(0, A.length);
    }

    private void quickSort(int start, int end) {
        if (end - start < 2)
            return;
        int p = partition(start, end);
        quickSort(start, p);
        quickSort(p+1, end);
    }

    private int partition(int start, int end) {
        int barrier = start;
        for (int i = start+1; i < end; ++i)
            if (order.compare(A[i], A[start]))
                swap(i, ++barrier);
        swap(start, barrier);
        return barrier;
    }

    private void swap(int i, int j) {
        Student tmp = A[j];
        A[j] = A[i];
        A[i] = tmp;
    }
}
